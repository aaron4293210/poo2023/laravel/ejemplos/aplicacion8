<?php

use App\Http\Controllers\AlumnoController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\PerteneceController;
use App\Http\Controllers\PracticaController;
use App\Http\Controllers\PresentaController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
})->name('inicio');

Route::resource('alumno', AlumnoController::class);
Route::resource('curso', CursoController::class);
Route::resource('practica', PracticaController::class);
Route::resource('pertenece', PerteneceController::class);
Route::resource('presenta', PresentaController::class);


// Enrutamiento adicional para practicas
Route::controller(PracticaController::class)->group(function () {
    Route::get('/practica/confirmar/{practica}', 'confirmar')->name('practica.confirmar');
});

// Enrutamiento adicional para Pertenece
Route::controller(PerteneceController::class)->group(function () {
    Route::get('/pertenece/confirmar/{pertenece}', 'confirmar')->name('pertenece.confirmar');
});

// Enrutamiento adicional para Presenta
Route::controller(PresentaController::class)->group(function () {
    Route::get('/presenta/confirmar/{presenta}', 'confirmar')->name('presenta.confirmar');
});
