<?php

namespace Database\Seeders;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Pertenece;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PerteneceSeeder extends Seeder {
    /**
     * Run the database seeds.
     */
    public function run(): void {
        for ($i = 0; $i < 10; $i++) {
            $curso = Curso::factory()->create();
            $alumno = Alumno::factory()->create();

            Pertenece::factory()
                ->for($curso)
                ->for($alumno)
                ->create();
        }
    }
}
