<?php

namespace App\Http\Controllers;

use App\Http\Requests\PracticaStoreRequest;
use App\Http\Requests\PracticaUpdateRequest;
use App\Models\Curso;
use App\Models\Practica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PracticaController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $practicas = Practica::all();

        return view('practica.index', compact('practicas'));
    }

    /**
     * Show the form for creating a new resource.
    */
    public function create() {
        $cursos = Curso::select('id', 'nombre')->get();
        $practica = new Practica();

        return view('practica.create', compact('cursos', 'practica'));
    }

    /**
     * Store a newly created resource in storage.
    */
    public function store(PracticaStoreRequest $request) {
        // Ver lo que se envia
        $ficheroEnviado = $request->file('fichero');
        $fichero = $ficheroEnviado->store('ficheros', 'public');

        $practica = new Practica();
            $practica->fill($request->all());
            $practica->fichero = $fichero;
        $practica->save();

        return redirect()
            ->route('practica.show', $practica)
            ->with('mensaje', 'Curso se ha creado con éxito')
        ;
    }

    /**
     * Display the specified resource.
    */
    public function show(Practica $practica) {
        return view('practica.show', compact('practica'));
    }

    /**
     * Show the form for editing the specified resource.
    */
    public function edit(Practica $practica) {
        $cursos = Curso::select('id', 'nombre')->get();

        return view('practica.edit', compact('practica', 'cursos'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PracticaUpdateRequest $request, Practica $practica) {
        // Si recibo un nuevo archivo
        if ($request->hasFile('fichero')) {
            // Recupero el archivo
            $ficheroEnviado = $request->file('fichero');

            // Elimino el archivo anterior
            Storage::disk('public')->delete($practica->fichero);

            // Almaceno el nuevo archivo
            $nuevoFichero = $ficheroEnviado->store('ficheros', 'public');

            // Actualizo el registro
            $practica->fill($request->all());
            $practica->fichero = $nuevoFichero;
        }
        else {
            // Si no hay nuevo archivo, actualizo el registro
            $practica->update($request->all());
        }

        // Guardo el registro
        $practica->save();

        return redirect()
            ->route('practica.show', $practica)
            ->with('mensaje', 'Practica actualizado con éxito')
        ;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Practica $practica) {
        $practica->delete();

        $ficheroEliminar = $practica->fichero;
        Storage::disk('public')->delete($ficheroEliminar);

        return redirect()
            ->route('practica.index')
            ->with('mensaje', 'Practica borrado con éxito')
        ;
    }

    public function confirmar(Practica $practica) {
        return view('practica.confirmar', compact('practica'));
    }
}
