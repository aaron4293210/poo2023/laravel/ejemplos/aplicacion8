<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlumnoStoreRequest;
use App\Http\Requests\AlumnoUpdateRequest;
use App\Models\Alumno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AlumnoController extends Controller {
    /**
     * Display a listing of the resource.
    */
    public function index() {
        $alumnos = Alumno::all();

        return view('alumno.index', compact('alumnos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        return view('alumno.create', [
            'alumno' => new Alumno(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
    */
    public function store(AlumnoStoreRequest $request) {
        $fotoEnviada = $request->file('foto');
        $foto = $fotoEnviada->store('fotos', 'public');

        $alumno = new Alumno();
            $alumno->fill($request->all());
            $alumno->foto = $foto;
        $alumno->save();

        return redirect()
            ->route('alumno.show', $alumno)
            ->with('mensaje', 'Alumno se ha creado con éxito')
        ;
    }

    /**
     * Display the specified resource.
    */
    public function show(Alumno $alumno) {
        return view('alumno.show', compact('alumno'));
    }

    /**
     * Show the form for editing the specified resource.
    */
    public function edit(Alumno $alumno) {
        return view('alumno.edit', compact('alumno'));
    }

    /**
     * Update the specified resource in storage.
    */
    public function update(AlumnoUpdateRequest $request, Alumno $alumno) {
        if ($request->hasFile('foto')) {
            $fotoEnviada = $request->file('foto');

            Storage::disk('public')->delete($alumno->foto);

            $nuevaFoto = $fotoEnviada->store('fotos', 'public');

            $alumno->fill($request->all());
            $alumno->foto = $nuevaFoto;
        }
        else {
            $alumno->update($request->all());
        }

        $alumno->save();

        return redirect()
            ->route('alumno.show', $alumno)
            ->with('mensaje', 'Alumno actualizado con éxito')
        ;
    }

    /**
     * Remove the specified resource from storage.
    */
    public function destroy(Alumno $alumno) {
        $fotoEnviada = $alumno->foto;
        Storage::disk('public')->delete($fotoEnviada);

        $alumno->delete();

        return redirect()
            ->route('alumno.index')
            ->with('mensaje', 'Alumno borrado con éxito')
        ;
    }
}
