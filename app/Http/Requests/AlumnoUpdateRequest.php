<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

class AlumnoUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array {
        return [
            'nombre' => 'required',
            'apellidos' => 'required',
            'fechanacimiento' => 'required|date',
            'email' => [
                'required',
                'email',
                'unique:alumnos,email,' . $this->alumno->id],
            'foto' => [
                File::types([
                'png', 'jpg', 'jpeg'])
                ->max(1024)],
        ];
    }

    public function messages(): array{
        return [
            'fechanacimiento.required' => 'La fecha de nacimiento es obligatoria',
        ];
    }
}
