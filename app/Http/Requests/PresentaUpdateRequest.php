<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PresentaUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array {
        return [
            'nota' => 'numeric',
            'practica_id' => 'required|exists:practicas,id',
            'alumno_id' => 'required|exists:alumnos,id',
        ];
    }

    public function messages(): array {
        return [
            'practica_id.required' => 'El campo practica es obligatorio',
            'practica_id.exists' => 'La practica no existe',
            'alumno_id.required' => 'El campo alumno es obligatorio',
            'alumno_id.exists' => 'El alumno no existe',
        ];
    }
}
