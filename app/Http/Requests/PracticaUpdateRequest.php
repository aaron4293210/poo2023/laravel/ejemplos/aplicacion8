<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

class PracticaUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array{
        return [
            'titulo' => 'required|max:200',
            'fichero' => [
                File::types([
                    'pdf'])
                    ->max(1024)],
            'curso_id' => 'required|exists:cursos,id',
        ];
    }

    public function messages(): array {
        return [
            'curso_id.required' => 'El campo curso es obligatorio',
            'curso_id.exists' => 'El curso no existe',
        ];
    }
}
