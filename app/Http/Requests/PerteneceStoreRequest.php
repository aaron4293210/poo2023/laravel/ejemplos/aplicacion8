<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PerteneceStoreRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array {
        return [
            'alumno_id' => 'required|exists:alumnos,id',
            'curso_id' => 'required|exists:cursos,id',
        ];
    }

    public function messages(): array {
        return [
            'alumno_id.required' => 'El alumno es obligatorio',
            'alumno_id.exists' => 'El alumno no existe',
            'curso_id.required' => 'El curso es obligatorio',
            'curso_id.exists' => 'El curso no existe',
        ];
    }
}
