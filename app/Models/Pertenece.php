<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Schema;

class Pertenece extends Model {
    use HasFactory;

    protected $table = 'perteneces';

    // Campos de asignación masiva
    protected $fillable = [
        'curso_id',
        'alumno_id',
    ];

    // Nombre de los campos  de las tablas
    public static $labels = [
        'id' => 'ID Pertenece',
        'curso_id' => 'ID Curso',
        'alumno_id' => 'ID Alumno',
        'nombrealumno' => 'Nombre Alumno',
        'nombrecurso' => 'Nombre Curso',
    ];

    /**
     ** Recupera la lista de campos de la tabla asociada al modelo.
     *
     * @return array La lista de nombres de campos.
    */
    public function getFields(): array {
        return Schema::getColumnListing($this->table);
    }

    /**
     ** Recupera la etiqueta de un atributo dado.
     *
     * @param string $attribute El nombre del atributo.
     * @return string La etiqueta del atributo, o el nombre del atributo si no se encuentra una etiqueta.
    */
    public function getAttributeLabel($attribute): string {
        return self::$labels[$attribute] ?? $attribute;
    }

    /**
     ** Obtiene la lista de campos de la modelo de tabla asociada.
     *
     * @return array|BelongsTo La lista de nombres de campos asociados a Alumno.
    */
    public function alumno() : BelongsTo {
        return $this->belongsTo(Alumno::class, 'alumno_id');
    }

    /**
     ** Obtiene la lista de campos de la modelo de tabla asociada.
     *
     * @return array|BelongsTo La lista de nombres de campos asociados a Practica.
    */
    public function curso() : BelongsTo {
        return $this->belongsTo(curso::class, 'curso_id');
    }

    /**
     ** Obtiene el valor del atributo "nombre" del modelo "alumno" asociado.
     *
     * @return string El valor del atributo "nombre".
    */
    public function getNombreAlumnoAttribute(): string {
        return $this->alumno->nombre;
    }

    /**
     ** Obtiene el valor del atributo "nombre" del modelo "curso" asociado.
     *
     * @return string El valor del atributo "nombre".
    */
    public function getNombreCursoAttribute(): string {
        return $this->curso->nombre;
    }
}
