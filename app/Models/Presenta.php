<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Schema;

class Presenta extends Model {
    use HasFactory;

    protected $table = 'presentas';

    // Campos de asignación masiva
    protected $fillable = [
        'alumno_id',
        'practica_id',
        'nota',
    ];

    // Nombre de los campos  de las tablas
    public static $labels = [
        'id' => 'ID Presenta',
        'alumno_id' => 'ID Alumno',
        'practica_id' => 'ID Practica',
        'nota' => 'Nota Alumno',
        'titulocurso' => 'Título de la Práctica',
        'nombrecurso' => 'Nombre Curso',
    ];

    /**
     ** Recupera la lista de campos de la tabla asociada al modelo.
     *
     * @return array La lista de nombres de campos.
    */
    public function getFields(): array {
        return Schema::getColumnListing($this->table);
    }

    /**
     ** Recupera la etiqueta de un atributo dado.
     *
     * @param string $attribute El nombre del atributo.
     * @return string La etiqueta del atributo, o el nombre del atributo si no se encuentra una etiqueta.
    */
    public function getAttributeLabel($attribute): string {
        return self::$labels[$attribute] ?? $attribute;
    }

    /**
     ** Obtiene la lista de campos de la modelo de tabla asociada.
     *
     * @return array|BelongsTo La lista de nombres de campos asociados a Alumno.
    */
    public function alumno() : BelongsTo {
        return $this->belongsTo(Alumno::class, 'alumno_id');
    }

    /**
     ** Obtiene la lista de campos de la modelo de tabla asociada.
     *
     * @return array|BelongsTo La lista de nombres de campos asociados a Practica.
    */
    public function practica() : BelongsTo {
        return $this->belongsTo(Practica::class, 'practica_id');
    }

    public function getTitulocursoAttribute(): string {
        return $this->practica->titulo;
    }

    public function getNombreCursoAttribute(): string {
        return $this->curso->nombre;
    }
}
