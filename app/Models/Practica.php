<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Practica extends Model {
    use HasFactory;

    protected $table = 'practicas';

    // Campos de asignación masiva
    protected $fillable = [
        'titulo',
        'fichero',
        'curso_id',
    ];

    // Nombre de los campos  de las tablas
    public static $labels = [
        'id' => 'ID Practica',
        'titulo' => 'Título Practica',
        'fichero' => 'Fichero Practica',
        'curso_id' => 'ID Curso',
        'nombrecurso' => 'Nombre Curso',
    ];

    /**
     ** Recupera la lista de campos de la tabla asociada al modelo.
    *
    * @return array La lista de nombres de campos.
    */
    public function getFields(): array {
        return Schema::getColumnListing($this->table);
    }

    /**
     ** Recupera la etiqueta de un atributo dado.
    *
    * @param string $attribute El nombre del atributo.
    * @return string La etiqueta del atributo, o el nombre del atributo si no se encuentra una etiqueta.
    */
    public function getAttributeLabel($attribute): string {
        return self::$labels[$attribute] ?? $attribute;
    }

    /**
     ** Obtiene la lista de campos de la modelo de tabla asociada.
    *
    * @return array|HasMany La lista de nombres de campos asociados a Presenta.
    */
    public function presentas() : HasMany {
        return $this->hasMany(Presenta::class);
    }

    /**
     ** Obtiene la lista de campos de la modelo de tabla asociada.
    *
    * @return array|HasMany La lista de nombres de campos asociados a Curso.
    */
    public function curso() : BelongsTo {
        return $this->belongsTo(Curso::class, 'curso_id');
    }

    /**
     ** Obtiene el valor del atributo "nombre" del modelo "curso" asociado.
    *
    * @return string El valor del atributo "nombre".
    */
    public function getNombreCursoAttribute(): string {
        return $this->curso->nombre;
    }
}
