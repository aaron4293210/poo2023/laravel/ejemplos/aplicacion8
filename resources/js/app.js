import './bootstrap';

import * as bootstrap from 'bootstrap';

let eliminar = document.querySelector('#eliminar')

if (eliminar) {
    eliminar.addEventListener('click', (event) => {
        event.preventDefault();

        let confirmar = false

        confirmar = window.confirm('¿Estás seguro de eliminar el registro?')

        if (confirmar) {
            eliminar.submit()
        }
    });
}

if (document.querySelector('#preview')) {

    document.querySelector('#fichero').addEventListener('change', (event) => {
        document.querySelector('#preview').src = window.URL.createObjectURL(event.target.files[0]);
    });
}
