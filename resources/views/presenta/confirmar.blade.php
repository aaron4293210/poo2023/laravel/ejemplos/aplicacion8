@extends('layouts.main')

@section('content')
    <h1>Eliminar Presenta</h1>

    <div>
        <a href="{{ route('presenta.index') }}" class="boton">Volver</a><br><br>
    </div>

    <div class="tarjeta">
        <ul>
            <li>ID: {{ $presenta->id }}</li>
            <li>Nota: {{ $presenta->nota }}</li>
            <li>Practica ID: {{ $presenta->practica_id }} - {{ $presenta->practica->titulo }}</li>
            <li>Alumno ID: {{ $presenta->alumno_id }} - {{ $presenta->alumno->nombre }}</li>
        </ul>

        <div class="botones">
            <form action="{{ route('presenta.destroy', $presenta)}}" method="POST" >
                @csrf @method('DELETE')
                <button class="boton" type="submit">Borrar</button>
            </form>
        </div>
    </div>

@endsection
