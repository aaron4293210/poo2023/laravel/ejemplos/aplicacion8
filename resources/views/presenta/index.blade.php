@extends('layouts.main')

@section('content')
    <h1>Listado Presenta</h1>

    <div>
        <a href="{{ route('presenta.create') }}" class="boton">Crear presenta</a><br><br>
    </div>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <div class="listado">
        @foreach ($presentas as $presenta)
            <div class="tarjeta">
                <ul>
                    <li><a href="{{ route('presenta.show', $presenta) }}" >ID:</a> {{ $presenta->id }}</li>
                    <li>Nota: {{ $presenta->nota }}</li>
                    <li>Practica ID: {{ $presenta->practica_id }} - {{ $presenta->practica->titulo }}</li>
                    <li>Alumno ID: {{ $presenta->alumno_id }} - {{ $presenta->alumno->nombre }}</li>
                </ul>

                <div class="botones">
                    @php
                        $id=$presenta;
                    @endphp
                    <a href="{{ route('presenta.show', $presenta) }}" class="boton">Ver</a>
                    <a href="{{ route('presenta.edit', $presenta) }}" class="boton">Editar</a>

                    <form action="{{ route('presenta.destroy', $presenta)}}" method="POST" id="eliminar">
                        @csrf @method('DELETE')
                        <button class="boton" type="submit">Borrar</button>
                    </form>

                    <a href="{{ route('presenta.confirmar', $presenta) }}" class="boton"> Eliminar 2 </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
