@extends('layouts.main')

@section('content')
    <h1>Eliminar Pertence</h1>

    <div>
        <a href="{{ route('practica.index') }}" class="boton">Volver</a><br><br>
    </div>

    <ul>
        <li><a href="{{ route('practica.show', $practica) }}" >ID:</a> {{ $practica->id }}</li>
        <li>Titulo: {{ $practica->titulo }}</li>
        <li>Fichero: {{ $practica->fichero }}</li>
        <li>Curso ID: {{ $practica->curso_id }} - {{ $practica->curso->nombre }}</li>
    </ul>

    <form action="{{ route('practica.destroy', $practica)}}" method="POST" >
        @csrf @method('DELETE')
        <button type="submit">Borrar</button>
    </form><br><br>
@endsection
