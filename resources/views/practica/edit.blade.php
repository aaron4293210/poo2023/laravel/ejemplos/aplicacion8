@extends('layouts.main')

@section('content')
    <h1>Editar Practica</h1>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p class="">{{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="">
            <div class="" style="background-color: gray">
                <h2 class="">Error</h2>
                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <br><a href="{{ route('practica.index') }}" class="boton">Volver</a><br><br>

    <form action="{{ route('practica.update', $practica) }}" method="POST"  enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('practica._form')
    </form><br><br>
@endsection
