@extends('layouts.main')

@section('content')
    <h1>Listado Practica</h1>

    <div>
        <a href="{{ route('practica.index') }}" class="boton">Volver</a><br><br>
    </div>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <div class="tarjeta">
        <ul>
            <li>ID: {{ $practica->id }}</li>
            <li>Titulo: {{ $practica->titulo }}</li>
            <li>Fichero: <a href="{{ asset('storage/' . $practica->fichero ) }}">{{ $practica->fichero }}</a></li>
            <li>Curso ID: {{ $practica->curso_id }} - {{ $practica->curso->nombre }}</li>
        </ul>

        <div class="botones">
            <a href="{{ route('practica.edit', $practica) }}" class="boton">Editar</a>
            <form action="{{ route('practica.destroy', $practica)}}" method="POST" id="eliminar" class="form-inline">
                @csrf @method('DELETE')
                <button class="boton" type="submit">Borrar</button>
            </form>
        </div>
    </div>
@endsection
