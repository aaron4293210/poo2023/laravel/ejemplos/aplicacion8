<div>
    <label for="titulo">Titulo</label>
    <input type="text" name="titulo" id="titulo" value="{{ old('titulo', $practica->titulo) }}">

    @error('titulo')
        <p style="color: #dc3545;"> {{ $message }} </p>
    @enderror
</div>

<div>
    <label for="fichero">Fichero</label>
    <div>
        {{ $practica->fichero }}
    </div>
    <input type="file" name="fichero" id="fichero" value="{{ old('fichero', $practica->fichero) }}">

    @error('fichero')
        <p style="color: #dc3545;"> {{ $message }} </p>
    @enderror
</div>

<div>
    <label for="curso_id">Curso ID</label>

    <select name="curso_id" id="curso_id">
        <option value="">-- Seleccione --</option>

        @foreach ($cursos as $curso)
            <option value="{{ $curso->id }}" {{ old('curso_id',$practica->curso_id) ==  $curso->id ? 'selected' : ''}} >{{ $curso->nombre }}</option>
        @endforeach
    </select>

    @error('curso_id')
        <p style="color: #dc3545;"> {{ $message }} </p>
    @enderror
</div>

<div>
    <button class="boton" type="submit">Enviar</button>
</div>
