@extends('layouts.main')

@section('content')
    <h1>Crear Practica</h1>

    <a href="{{ route('practica.index') }}" class="boton">Volver</a>

    <form action="{{ route('practica.store') }}" method="POST"  enctype="multipart/form-data">
        @csrf

        @include('practica._form')
    </form><br>
@endsection
