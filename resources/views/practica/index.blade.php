@extends('layouts.main')

@section('content')
    <h1>Listado Practicas</h1>

    <div>
        <a href="{{ route('practica.create') }}" class="boton">Crear practica</a><br><br>
    </div>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <div class="listado">
        @foreach ($practicas as $practica)
            <div class="tarjeta">
                <ul>
                    <li><a href="{{ route('practica.show', $practica) }}" >ID:</a> {{ $practica->id }}</li>
                    <li>Titulo: {{ $practica->titulo }}</li>
                    <li>Fichero: {{ $practica->fichero }}</li>
                    <li>Curso ID: {{ $practica->curso_id }} - {{ $practica->curso->nombre }}</li>
                </ul>


                <div class="botones">
                    <a href="{{ route('practica.show', $practica) }}" class="boton">Ver</a>
                    <a href="{{ route('practica.edit', $practica) }}" class="boton">Editar</a>

                    <form action="{{ route('practica.destroy', $practica)}}" method="POST" class="form-inline">
                        @csrf @method('DELETE')
                        <button class="boton" type="submit">Borrar</button>
                    </form>
                    <a href="{{ route('practica.confirmar', $practica) }}" class="boton"> Eliminar 2 </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
