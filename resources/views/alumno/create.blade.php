@extends('layouts.main')

@section('content')
    <h1>Crear Alumno</h1>

    <a href="{{ route('alumno.index') }}" class="boton">Volver</a>

    <form action="{{ route('alumno.store') }}" method="POST"  enctype="multipart/form-data">
        @csrf

        @include('alumno._form')
    </form>
@endsection

@section('css')
    <style>
        .imagen {
            max-width: 100%;
            width: 300px;
            height: 300px;
            border: 1px solid #ddd;
        }
    </style>
@endsection
