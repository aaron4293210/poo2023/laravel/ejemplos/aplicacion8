@extends('layouts.main')

@section('content')
    <h1>Editar alumno</h1>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p class="">{{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="">
            <div class="" style="background-color: gray">
                <h2 class="">Error</h2>
                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <a href="{{ route('alumno.index') }}" class="boton">Volver</a>

    <form action="{{ route('alumno.update', $alumno) }}" method="POST"  enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('alumno._form')
    </form>
@endsection

@section('css')
    <style>
        .imagen {
            max-width: 100%;
            width: 300px;
            height: 300px;
            border: 1px solid #ddd;
        }
    </style>
@endsection
