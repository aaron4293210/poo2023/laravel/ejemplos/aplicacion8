<div>
    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" id="nombre" value="{{ old('nombre', $alumno->nombre) }}">
    @error('nombre')
        <p style="color: #dc3545;"> {{ $message }} </p>
    @enderror
</div>

<div>
    <label for="apellidos">Apellidos</label>
    <input type="text" name="apellidos" id="apellidos" value="{{ old('apellidos', $alumno->apellidos) }}">
    @error('apellidos')
        <p style="color: #dc3545;"> {{ $message }} </p>
    @enderror
</div>

<div>
    <label for="fechanacimiento">Fecha de Nacimiento</label>
    <input type="date" name="fechanacimiento" id="fechanacimiento" value="{{ old('fechanacimiento', $alumno->fechanacimiento) }}">
    @error('fechanacimiento')
        <p style="color: #dc3545;"> {{ $message }} </p>
    @enderror
</div>

<div>
    <label for="email">Email</label>
    <input type="email" name="email" id="email" value="{{ old('email', $alumno->email) }}">
    @error('email')
        <p style="color: #dc3545;"> {{ $message }} </p>
    @enderror
</div>

<div>
    <label for="foto">Foto</label>
    @if ($alumno->fotos)
        <img id="preview" class="imagen" src="{{ asset('storage/' . $alumno->foto ) }}" alt="{{ $alumno->foto }}">
    @else
        <img id="preview" class="imagen" src="{{ asset('storage/' . $alumno->foto ) }}">
    @endif

    <br><br>

    <input type="file" name="foto" id="fichero" value="{{ old('fotos', $alumno->foto) }}">
    @error('foto')
        <p style="color: #dc3545;"> {{ $message }} </p>
    @enderror
</div><br>

<div>
    <button class="boton" type="submit">Actualizar</button>
</div>
