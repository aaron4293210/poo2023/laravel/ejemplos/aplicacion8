@extends('layouts.main')

@section('content')
    <h1>Listado Alumnos</h1>

    <div>
        <a href="{{ route('alumno.index') }}" class="boton">Volver</a><br><br>
    </div>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <div class="tarjeta">
        <ul>
            <li>ID: {{ $alumno->id }}</li>
            <li>Nombre: {{ $alumno->nombre }}</li>
            <li>Apellidos: {{ $alumno->apellidos }}</li>
            <li>Fecha Nacimiento: {{ $alumno->fechanacimiento }}</li>
            <li>Email: {{ $alumno->email }}</li>
            <li>Foto: <br>
                <img class="imagen" src="{{ asset('storage/' . $alumno->foto ) }}" alt="{{ $alumno->foto }}">
            </li>

            <div class="botones">
                <a class="boton" href="{{ route('alumno.edit', $alumno) }}">Editar</a>

                <form action="{{ route('alumno.destroy', $alumno)}}" method="POST" id="eliminar">
                    @csrf @method('DELETE')
                    <button class="boton" type="submit">Borrar</button>
                </form>
            </div>
        </ul>
    </div>
@endsection

@section('css')
    <style>
        .imagen {
            max-width: 100%;
            width: 300px;
            height: 300px;
            border: 1px solid #ddd;
            border-radius: 50%;
        }
    </style>
@endsection
