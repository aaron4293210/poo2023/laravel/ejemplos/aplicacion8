@extends('layouts.main')

@section('content')
    <h1>Listado Alumnos</h1>

    <div>
        <a href="{{ route('alumno.create') }}" class="boton">Crear Alumno</a><br><br>
    </div>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <div class="listado">
        @foreach ($alumnos as $alumno)
            <div class="tarjeta">
                <ul>
                    <li><a href="{{ route('alumno.show', $alumno) }}" >ID:</a> {{ $alumno->id }}</li>
                    <li>Nombre: {{ $alumno->nombre }}</li>
                    <li>Apellidos: {{ $alumno->apellidos }}</li>
                    <li>Fecha Nacimiento: {{ $alumno->fechanacimiento }}</li>
                    <li>Email: {{ $alumno->email }}</li>
                </ul>

                <div class="botones">
                    <a href="{{ route('alumno.show', $alumno) }}" class="boton">Ver</a>
                    <a href="{{ route('alumno.edit', $alumno) }}" class="boton">Editar</a>
                    <form action="{{ route('alumno.destroy', $alumno)}}" method="POST" id="eliminar" class="form-inline">
                        @csrf @method('DELETE')
                        <button class="boton" type="submit">Borrar</button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
@endsection
