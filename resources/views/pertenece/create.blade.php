@extends('layouts.main')

@section('content')
    <h1>Crear Pertenece</h1>

    @if ($errors->any())
        <div class="">
            <div class="" style="">
                <h2 class="">Error</h2>

                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <a href="{{ route('pertenece.index') }}" class="boton">Volver</a>

    <form action="{{ route('pertenece.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div>
            <label for="alumno_id">Alumno ID</label>

            <select name="alumno_id" id="alumno_id">
                <option value="">-- Seleccione --</option>

                @foreach ($alumnos as $alumno)
                    <option value="{{ $alumno->id }}">{{ $alumno->nombrecompleto }}</option>
                @endforeach
            </select>
        </div>

        <div>
            <label for="curso_id">Curso ID</label>

            <select name="curso_id" id="curso_id">
                <option value="">-- Seleccione --</option>

                @foreach ($cursos as $curso)
                    <option value="{{ $curso->id }}">{{ $curso->nombre }}</option>
                @endforeach
            </select>
        </div>

        <div>
            <button class="boton" type="submit">Crear</button>
        </div>
    </form>
@endsection
