@extends('layouts.main')

@section('content')
    <h1>Eliminar Pertenece</h1>

    <div>
        <a href="{{ route('pertenece.index') }}" class="boton">Volver</a><br><br>
    </div>

    <div class="listtado">
        <div class="tarjeta">
            <ul>
                <li><a href="{{ route('pertenece.show', $pertenece) }}" >ID:</a> {{ $pertenece->id }}</li>
                <li>Alumno ID: {{ $pertenece->alumno_id }} - {{ $pertenece->alumno->nombre }}</li>
                <li>Curso ID: {{ $pertenece->curso_id }} - {{ $pertenece->curso->nombre }}</li>
            </ul>

            <div class="botones">
                <form action="{{ route('pertenece.destroy', $pertenece)}}" method="POST" class="form-inline">
                    @csrf @method('DELETE')
                    <button class="boton" type="submit">Borrar</button>
                </form>
            </div>
        </div>
    </div>
@endsection
