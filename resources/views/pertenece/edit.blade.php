@extends('layouts.main')

@section('content')
    <h1>Editar Pertenece</h1>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p class="">{{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="">
            <div class="" style="background-color: gray">
                <h2 class="">Error</h2>
                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <br><a href="{{ route('pertenece.index') }}" class="boton">Volver</a><br><br>

    <form action="{{ route('pertenece.update', $pertenece) }}" method="POST"  enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div>
            <label for="alumno_id">Alumno ID</label>

            <select name="alumno_id" id="alumno_id">
                @foreach ($alumnos as $alumno)
                    <option value="{{ $alumno->id }}" {{ $pertenece->alumno_id == $alumno->id ? 'selected' : '' }}>{{ $alumno->nombrecompleto }}</option>
                @endforeach
            </select>
        </div><br>

        <div>
            <label for="curso_id">Curso ID</label>

            <select name="curso_id" id="curso_id">
                @foreach ($cursos as $curso)
                    <option value="{{ $curso->id }}" {{ $pertenece->curso_id == $curso->id ? 'selected' : '' }}>{{ $curso->nombre }}</option>
                @endforeach
            </select>
        </div><br>

        <div>
            <button class="boton" type="submit">Actualizar</button>
        </div>
    </form>
@endsection
