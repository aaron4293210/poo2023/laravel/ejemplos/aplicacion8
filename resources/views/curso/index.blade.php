@extends('layouts.main')

@section('content')
    <h1>Listado Curso</h1>

    <div>
        <a href="{{ route('curso.create') }}" class="boton">Crear Curso</a><br>
    </div>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <div class="listado">
        @foreach ($cursos as $curso)
            <div class="tarjeta">
                <ul>
                    <li><a href="{{ route('curso.show', $curso) }}" >ID:</a> {{ $curso->id }}</li>
                    <li>Nombre: {{ $curso->nombre }}</li>
                    <li>Duracion: {{ $curso->duracion }}</li>
                    <li>Fecha Comienzo: {{ $curso->fechacomienzo }}</li>
                    <li>Observaciones: {{ $curso->observaciones }}</li>
                </ul>

                <div class="botones">
                    <a class="boton" href="{{ route('curso.show', $curso) }}">Ver</a>
                    <a class="boton" href="{{ route('curso.edit', $curso) }}">Editar</a>

                    <form action="{{ route('curso.destroy', $curso)}}" method="POST" id="eliminar">
                        @csrf @method('DELETE')
                        <button class="boton" type="submit">Borrar</button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
@endsection
