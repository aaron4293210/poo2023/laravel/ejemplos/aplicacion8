@extends('layouts.main')

@section('content')
    <h1>Crear Curso</h1>

    @if ($errors->any())
        <div class="">
            <div class="" style="">
                <h2 class="">Error</h2>

                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <a href="{{ route('curso.index') }}" class="boton">Volver</a>

    <form action="{{ route('curso.store') }}" method="POST"  enctype="multipart/form-data">
        @csrf

        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre">
        </div>

        <div>
            <label for="duracion">Duracion</label>
            <input type="number" name="duracion" id="duracion">
        </div>

        <div>
            <label for="fechacomienzo">Fecha de Comienzo</label>
            <input type="date" name="fechacomienzo" id="fechacomienzo">
        </div>

        <div>
            <label for="observaciones">Observaciones</label>
            <input type="text" name="observaciones" id="observaciones">
        </div>

        <div>
            <button class="boton" type="submit">Crear</button>
        </div>
    </form>
@endsection
